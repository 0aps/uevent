/**
 * Passport configuration
 *
 * This if the configuration for your Passport.js setup and it where you'd
 * define the authentication strategies you want your application to employ.
 *
 * I have tested the service with all of the providers listed below - if you
 * come across a provider that for some reason doesn't work, feel free to open
 * an issue on GitHub.
 *
 * Also, authentication scopes can be set through the `scope` property.
 *
 * For more information on the available providers, check out:
 * http://passportjs.org/guide/providers/
 */

module.exports.passport = {

  facebook: {
    name: 'Facebook',
    image: 'log-in-with-facebook.png',
    protocol: 'oauth2',
    strategy: require('passport-facebook').Strategy,
    scope: [ 'email', 'user_friends' ],
    options: {
      clientID: '844782102239377',
      clientSecret: '42cff641cf8fdd474f11de87f362f6e3',
    }
  },

  google: {
    name: 'Google',
    image: 'sign-in-with-google.png',
    protocol: 'oauth2',
    strategy: require('passport-google-oauth').OAuth2Strategy,
    scope: [ 'email', 'profile'],
    options: {
      clientID: '43187203292-ab84tdp3o987gmipd952s2t0r8hicavb.apps.googleusercontent.com',
      clientSecret: 'X4zo6y7ALP-Vavgz-qFxs7Sp'
    }
  }
};
