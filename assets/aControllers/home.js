app.controller('HomeCtrl', function($scope, $location, authenticationFac, auth) {

    $scope.userInfo = auth.userName;

    $scope.logout = function () {

        authenticationFac.logout()
            .then(function (result) {
                $scope.userInfo = null;
                $location.path("/login");

            }, function (error) {
                console.log(error);
        });
    };
	
});