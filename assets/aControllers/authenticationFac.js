app.factory("authenticationFac", ["$http","$q","$window",function ($http, $q, $window) {

    var userInfo;

    function login(provider) {

        var deferred = $q.defer();
        
        $http.jsonp("/auth/"+provider+"?callback=JSON_CALLBACK")
        .success(function (data) {

            userInfo = {
                accessToken: "abc123", //data[0].access_token,
                userName: data
            };

            $window.sessionStorage["userInfo"] = JSON.stringify(userInfo);
            deferred.resolve(userInfo);

        }).error( function(error) {

            deferred.reject(error);

        });

        return deferred.promise;
    }

    function logout() {

        var deferred = $q.defer();

        $http.post("/api/logout", { headers: {"access_token": userInfo.accessToken }  })
        .then(function (result) {

            userInfo = null;
            $window.sessionStorage["userInfo"] = null;
            deferred.resolve(result);
    
        }, function (error) {

           deferred.reject(error);
        
        });

        return deferred.promise;
    }

    function getProviders() {

        var deferred = $q.defer();

        var providers = {},
            errors = {};

       $http.get("/api/login").success(function (data) {  

            providers = data;
            deferred.resolve(providers);

        }).error( function(data) {

           errors = data;
           deferred.reject(errors);

        });

        return deferred.promise;
    }

    function getUserInfo() {

        return userInfo;

    }

    function init() {

        if ($window.sessionStorage["userInfo"]) {
            userInfo = JSON.parse($window.sessionStorage["userInfo"]);
        }
        
    }
    init();

    return {
        login: login,
        logout: logout,
        getUserInfo: getUserInfo,
        getProviders : getProviders
    };
}]);
