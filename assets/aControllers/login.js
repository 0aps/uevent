app.controller('LoginCtrl', function($scope, $location, $window, authenticationFac) {

    $scope.userInfo = null,
    $scope.providers = {},
    $scope.errors = {};
    
    authenticationFac.getProviders()
    		.then(function(result){
       			$scope.providers = result;
        	}, function(error){
   	    		$scope.errors = error;			   	
    });
    
    $scope.login = function (provider) {

    authenticationFac.login(provider)
        .then(function (result) {

            $scope.userInfo = result;
            $location.path("/");

        }, function (error) {

            $window.alert("Invalid credentials");
            console.log(error);
        });
    };
});