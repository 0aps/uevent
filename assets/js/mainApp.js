var app = angular.module('UberEvent', ['ngRoute', 'ngResource']);

app.config(function ($routeProvider, $httpProvider) {

  $routeProvider
    .when('/', {
      controller:'HomeCtrl',
      templateUrl:'views/auth/homepage.html',
      resolve: { auth: manage }
    })
    .when('/login', {
      controller:'LoginCtrl',
      templateUrl:'views/auth/login.html',
      resolve: { auth: manage }
    })
    .otherwise({
      redirectTo:'/'
    });

});

app.run(["$rootScope", "$location", function ($rootScope, $location) {

    $rootScope.$on("$routeChangeSuccess", function (userInfo) {
        //console.log(userInfo);
    });

    $rootScope.$on("$routeChangeError", function (event, current, previous, eventObj) {

      if( eventObj.authenticated === true &&
                  eventObj.login === true)
            $location.path("/");

       else if (eventObj.authenticated === false ) {
            $location.path("/login");
        }
    });
}]);


function manage($q, $location, authenticationFac) {

  var userInfo = authenticationFac.getUserInfo();

    if ( (userInfo && $location.path() !== "/login") ||
         (!userInfo && $location.path() === "/login") )
      return $q.when(userInfo);
 
       //if you're log in can't do it again.
    if( $location.path() === "/login" )
        return $q.reject( {authenticated: true, login: true} );

    return $q.reject({ authenticated: false });
};